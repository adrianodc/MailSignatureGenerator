export class SignatureModel {
    name: string;
    occupation: string;
    telephone: string;
    email: string;
    imageURL: string;
    companyURL: string;
    companyName: string;
    facebookURL: string;
    linkedinURL: string;
}
