import { Component } from '@angular/core';
import { SignatureModel } from '../signature.model';
import { SignatureStringService } from '../signature-string.service';

@Component({
  selector: 'app-signature-generator',
  templateUrl: './signature-generator.component.html',
  styleUrls: ['./signature-generator.component.scss']
})
export class SignatureGeneratorComponent {
  public signature: SignatureModel = new SignatureModel();
  public generatedHTML: string;

  constructor(private signatureStringService: SignatureStringService) {
  }

  generateSignature(): void {
    this.generatedHTML = this.signatureStringService.generateString(this.signature);
  }
}
