import { TestBed } from '@angular/core/testing';

import { SignatureStringService } from './signature-string.service';

describe('SignatureStringService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignatureStringService = TestBed.get(SignatureStringService);
    expect(service).toBeTruthy();
  });
});
