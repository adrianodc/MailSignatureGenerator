import { Injectable } from '@angular/core';
import { SignatureModel } from './signature.model';

@Injectable({
  providedIn: 'root'
})
export class SignatureStringService {
  constructor() { }

  generateString(details: SignatureModel): string {
    return `<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"
        xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
        xmlns="http://www.w3.org/TR/REC-html40">
      <head>
        <meta name=Generator content="Microsoft Word 15 (filtered medium)">
        <!--[if !mso]>
        <style>v\:* {behavior:url(#default#VML);}
            o\:* {behavior:url(#default#VML);}
            w\:* {behavior:url(#default#VML);}
            .shape {behavior:url(#default#VML);}
        </style>
        <![endif]-->
        <style>
            <!--
              /* Font Definitions */
              @font-face
                {font-family:"Cambria Math";
                panose-1:2 4 5 3 5 4 6 3 2 4;}
              @font-face
                {font-family:Calibri;
                panose-1:2 15 5 2 2 2 4 3 2 4;}
              /* Style Definitions */
              p.MsoNormal, li.MsoNormal, div.MsoNormal
                {margin:0in;
                margin-bottom:.0001pt;
                font-size:11.0pt;
                font-family:"Calibri",sans-serif;
                mso-fareast-language:EN-US;}
              a:link, span.MsoHyperlink
                {mso-style-priority:99;
                color:#0563C1;
                text-decoration:underline;}
              a:visited, span.MsoHyperlinkFollowed
                {mso-style-priority:99;
                color:#954F72;
                text-decoration:underline;}
              span.EmailStyle17
                {mso-style-type:personal-compose;
                font-family:"Calibri",sans-serif;
                color:windowtext;}
              .MsoChpDefault
                {mso-style-type:export-only;
                font-family:"Calibri",sans-serif;
                mso-fareast-language:EN-US;}
              @page WordSection1
                {size:8.5in 11.0in;
                margin:1.0in 1.0in 1.0in 1.0in;}
              div.WordSection1
                {page:WordSection1;}
              -->
        </style>
        <!--[if gte mso 9]>
        <xml>
            <o:shapedefaults v:ext="edit" spidmax="1026" />
        </xml>
        <![endif]--><!--[if gte mso 9]>
        <xml>
            <o:shapelayout v:ext="edit">
              <o:idmap v:ext="edit" data="1" />
            </o:shapelayout>
        </xml>
        <![endif]-->
      </head>
      <body lang=EN-GB link="#0563C1" vlink="#954F72">
        <div class=WordSection1>
            <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%">
              <tr>
                  <td style='padding:0in 0in 3.75pt 0in'>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'></td>
                        </tr>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'>
                              <p class=MsoNormal>
                                <b><span
                                    style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>
                                      ${details.name}</span></b>
                                <span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>
                                    <o:p></o:p>
                                </span>
                              </p>
                          </td>
                        </tr>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'>
                              <p class=MsoNormal>
                                <span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>
                                    ${details.occupation}
                                  <o:p></o:p>
                                </span>
                              </p>
                          </td>
                        </tr>
                    </table>
                  </td>
              </tr>
              <tr>
                  <td valign=top style='border:none;border-top:solid darkgray 1.0pt;padding:3.75pt 0in 3.75pt 0in'>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'>
                              <p class=MsoNormal>
                                <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>Mob:</span>
                                </b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>&nbsp;
                                <a href="tel:${details.telephone}" target="_blank"><span style='color:blue'>${details.telephone}</span></a></span>
                                <b>
                                    <span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>
                                      <o:p></o:p>
                                    </span>
                                </b>
                              </p>
                              <p class=MsoNormal>
                                <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>Email:</span>
                                </b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>&nbsp;</span>
                                <u><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:blue;mso-fareast-language:EN-GB'>
                                <a href="mailto:${details.email}"><span style='color:blue'>${details.email}</span></a></span></u>
                                <span style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-language:EN-GB'>
                                    <o:p></o:p>
                                </span>
                              </p>
                          </td>
                        </tr>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'>
                              <p class=MsoNormal>
                                <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>Web:</span></b>
                                <span style='font-size:10.0pt;font-family:"Arial",sans-serif;color:#333333;mso-fareast-language:EN-GB'>&nbsp;
                                <a href="${details.companyURL}"><span style='color:blue'>${details.companyURL}</span></a></span>
                                <span style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-language:EN-GB'>
                                    <o:p></o:p>
                                </span>
                              </p>
                          </td>
                        </tr>
                        <tr>
                          <td style='padding:0in 0in 0in 0in'>
                          <a href="${details.facebookURL}">  
                          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAC4klEQVRIieVXMWtUQRD+vuNxxXGEEILGwiZFChELLYSUFhbiPxBU5HYhWIkEC0tLsbIJ+1IINjZiEYmNSIKQTssgwVotJBwhHCmO+yxu37nZzHtHsEjhwHG3szPzzczOzuxREM6CWmeCCqCo23A9N0OyU7cvjTNFEpJAcsKPvCHJfijD0NJnmmrXc22SywBWAFwFUCSGJsabnElkRpL6JF8DeBPK8KsW2Dt/F8ALSfMW2DTwmiwMJb0nuZKCT87Y9dySpCcVaGWgCcCK1kh9QfI2gPup/ASY5C2Si01gDQ4dkfwOYAfAFsmt+PsgOlIAuOed71YKaXFdBNA2UQ3whPYlrQF4R/IbgEGU7ZL8IGk56nQAnANwmAMXklq54TSFOU/SCMAzkiGUYZDqeecHsbIrnRbJSWC118kCzKMmuQvgVQUaU3lF0jyEguRcnf0JcH4X0+/8jib0FcBRBG1JuknyOYBZjOunW6P3F7iqxrp1Xslx/RvAKLJaAK5LWswdzQM6BpxGlq8tj+N6lPHahoy5Ns/YaoEJHSKmV9Ig2xtgnAVgnIEZxO43FbgJNAKthjKs5XqxJz+NH7ieWyL5FsBlK7gT0ykFOk27NOzMSerkdiqqvU7/SrH1dqw0nwo4KrYAzHvnFyO7H8qwX8l45+ckzUb5JQDdumMzHwLWNZIESW1JDwFsSNoA8MA7X0TQQlKP5Ebcf1yl2qITDcRqJFXEse0tSFqIMuclpYPmgqRLdY+DlBqfPk3FFTNg6uWdz5JPvTXBLIPT5rXleN7304hHOViToiVjyebPIQt4IGnU1DZzsniWfpQbIs7qHPgLyYPKYH4mlkFrZNbpA9iT9MMC/iRpJzWaTijrvK0Cs+Z4DOhluV6eTHUoQx/AIwCbAPrxdWEW25T3V0pHAPYArAL4mG4c61zlernnnb8D4AbJa5I6JFspQAb6uXIQ48LZruQxPtOfADZDGXZzj/jf/Xf6A6cz4qWq/4lqAAAAAElFTkSuQmCC"
                            alt="Facebook Page of ${details.companyName}"></a>&nbsp;|&nbsp;
                            <a href="${details.linkedinURL}">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAADH0lEQVRIicWXsYsdVRjFf2dZHssWYVlkCwlb6GqhhUEipLDQVgiEQDBFCsF3BwTBWFilsIgWFiJoIdwJioV/QCBYiQRJk8VSlmAhCCGGJYSwSFjC4x2LnRm+d9/My4uFe5uZ99073znfud89M0/GHMdYORZUYLUMVKlaBV6x/bqkdRpytpH0LLknwD3g11znR+WkotRVqraAK7bPSdrsA12GgN3lPJS0Z/uqpJ9znaftRFdxlao14CpQxQSSlgZtASW1a9eBN4EauADstmvjHp+xfS4mHwIJFc3EWpI9Cm0DH8X1EfhtSZsB7KBhuAv88zRCLWA538Ztn2m2cg74hO1OettfA2eBs7avSXrSV2lb1aK5RokRsNEHPHO0JH2b67yf67wv6TpwP8oXE/dVXVbf/O4wFp3jjXC/ZnsUK4hgsQHjdUgFCF1dNgRwJY3TF5JWgA8kbZWgPV3cu899p6EDLhlKuhi6fM32jDoBYGr7oaR7wFTSBnCyzB2fmQHuYfmdpL+a+1clnQfuc3Qm27Uv2n5Z0pfAru1DYAd4H7gkaW0o/5xlBml+yHW+BVCl6jzwDnA31/mzdl2VqtOSngC/B1d6UKXqT2AEXAJW+uSfa65n8eRc59+APeC5KlUnQnwf+B64O/TsHPAylliM92z/aLtO47Qd4nds7w/l7JV6aJQJ0jjtCH0ObEma2L4DfNpMP5D0uE9mWPJ9XJpCIHLS9nqTdJWjxgIg13liexL9+6nApQUOmYHtleLsjgpig8XMGchQcy2KLfN+/k9SxwSLYossslSxA17EPrpUOVf69tCIfj4DDEyHWC/7qVM+21YZ+mD+0wd4DEwktbF3q1Sdbu5fsz2S9Hwap8sdIfQSRw7VEtxJ43S5Uw9ttwQkTRqMWWDbtyUdAJsNuw9jtc31BUlfLSj+lKRT5Yloxl6uc+dkcY9vArfaxeX5G5Ixzkdpi+15BHwTAx1wrvOB7Y+BG7Yf2p4WjGdAFn0MhHFo+w9JnwC/xAmVf2HSOG0Ab0l6o3GluffwUOcX8QnwN/BTrvNeuX4O+P8ax/bf6V/yDNTbalfgkQAAAABJRU5ErkJggg=="
                            alt="LinkedIn Page of ${details.companyName}"></a>
                          </td>
                        </tr>
                    </table>
                  </td>
                  <td style='border:none;border-top:solid darkgray 1.0pt;padding:3.75pt 0in 3.75pt 15.0pt'>
                    <div align=right>
                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0>
                          <tr>
                              <td style='padding:0in 0in 0in 0in'>
                                <p class=MsoNormal>
                                <a href="${details.companyURL}" style="mso-style-priority: 99;color: #0563C1;text-decoration: underline;">
                                <div style="max-width: 207px; max-height: 100px; font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;color:blue;mso-fareast-language:EN-GB;text-decoration:none">
                                  <img border="0" style="max-width: 100%; max-height: 100%;" id="companyLogo" src="${details.imageURL}" alt="${details.companyName}"></div></a>
                                    <span style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-language:EN-GB'>
                                      <o:p></o:p>
                                    </span>
                                </p>
                              </td>
                          </tr>
                        </table>
                    </div>
                  </td>
              </tr>
              <tr style='height:46.5pt'>
                  <td colspan=2 style='border:none;border-top:solid darkgray 1.0pt;padding:2.25pt 0in 0in 0in;height:46.5pt'>
                    <p class=MsoNormal style='text-align:justify'>
                        <span style='font-size:8.0pt;font-family:"Arial",sans-serif;color:#888888;mso-fareast-language:EN-GB'>
                        This message contains confidential information and is intended only for the intended recipients. If you are not an intended recipient you should not disseminate, distribute or copy this e-mail. Please notify us immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. E-mail transmission cannot be guaranteed to be secure or error-free as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. Therefore we do not accept liability for any errors or omissions in the contents of this message, which arise as a result of e-mail transmission. If verification is required please request a hard-copy version.</span>
                        <span style='font-size:8.0pt;font-family:"Times New Roman",serif;color:#888888;mso-fareast-language:EN-GB'>
                          <o:p></o:p>
                        </span>
                    </p>
                  </td>
              </tr>
            </table>
            <p class=MsoNormal>
              <span style='mso-fareast-language:EN-GB'>
                  <o:p>&nbsp;</o:p>
              </span>
            </p>
            <p class=MsoNormal>
              <o:p>&nbsp;</o:p>
            </p>
        </div>

      </body>
    </html>`;
  }
}
