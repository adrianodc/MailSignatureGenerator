import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ClipboardModule } from 'ngx-clipboard';

import { AppComponent } from './app.component';
import { SignatureGeneratorComponent } from './signature-generator/signature-generator.component';

@NgModule({
  declarations: [
    AppComponent,
    SignatureGeneratorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
